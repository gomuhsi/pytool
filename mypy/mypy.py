#!/usr/bin/env python
# coding: utf-8

helpMsg="""プログラム名:mypy

説明:
標準入力を読み込んだデータを１行単位で、
Pythonのスクリプトを実行するためのプログラム。

引数１:pythonの計算式.計算結果はprint関数の引数となる
引数２：ループ処理前に実行されるPythonスクリプト
引数３：ループ処理後に実行されるPythonスクリプト

オプション:
 -e:引数１を式としてではなく、Pythonスクリプトとして実行される
    また、print関数の引数としても用いられれない

処理記述の際の条件:
１行のデータは変数xに格納されている。
1行を正規表現の\sによってsplitされたリストが変数aに格納されている。
sys,reは事前にimportされるため使用可能スクリプト内で使用可能
数値の文字列を数値として扱うためにはint()などを用いる"""


import sys
import re

#if len(sys.argv) == 2:
# eval(sys.argv[1])

#for x in sys.argv[1:]:
# eval(sys.argv[1])


a=[]

flag_e=0
if "-e" in sys.argv:
 flag_e=1
 sys.argv.remove("-e") 

#helpフラグの確認
help_check= len( set(["-h","--help"]) & set(sys.argv))
if not help_check == 0:
 print(helpMsg)
 exit()


#事前処理の実行
try:
 exec(sys.argv[2])
except IndexError:
 pass

#if len(sys.argv)==2:
data = sys.stdin.read()
 #print(data)
data=data.strip("\n")
 #print(data.split("\n"))
for x in data.split("\n"):
 a=re.split("\s+",x)
  #print(exec(sys.argv[1]))
 try:
  if flag_e==0:
   print(eval(sys.argv[1]))
  else:
   exec(sys.argv[1])
 except:
  pass

#ループ後の処理
try:
 exec(sys.argv[3])
except IndexError:
 pass







